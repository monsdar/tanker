# Install Base system
* Install [Raspberry Pi OS lite](https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-64-bit) on your RPi
* `sudo apt-get update && sudo apt-get upgrade`
* Install Python and Pip
    * `sudo apt-get install python3-distutils python3-apt`
    * `curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`
    * `python3 get-pip.py --user`
    * `export PATH=$PATH:/home/$USER/.local/bin`
    * Check versions:
        * `python --version` (currently `Python 3.9.2`)
        * `pip --version` (currently `pip 22.2.2`)
* Install Ansible
    * `pip install --user ansible`
    * Check version: `ansible --version` (currently `ansible [core 2.13.4]`)
* Install Docker
    * `curl -fsSL https://get.docker.com -o get-docker.sh`
    * `sudo sh get-docker.sh`
    * `sudo usermod -aG docker $USER`
    * `newgrp docker`
    * `sudo apt-get install docker-compose-plugin`
    * `pip install docker-compose`
    * Check versions:
        * `docker version` (currently `Version: 20.10.18`)
        * `docker compose version` (currently `version v2.10.2`)
        * `docker-compose version` (currently `version 1.29.2`)
* Install tanker
    * `sudo apt-get install git`
    * `git clone https://gitlab.com/monsdar/tanker.git`
    * `cd tanker`
    * Passwords and tokens are stored in .env files so they do not have to be part of this repo:
        * `echo OPENHAB_INFLUX_PASSWORD=FOOBAR > grafana/.env`
        * `echo INFLUX_TOKEN=MY_INFLUX_TOKEN >> grafana/.env`
        * `echo INFLUX_TOKEN=MY_INFLUX_TOKEN > telegraf/.env`
        * `echo INFLUX_TOKEN=MY_INFLUX_TOKEN > influx/.env`
        * `echo INFLUX_ADMIN_PASSWORD=MY_INFLUX_PASSWORD >> influx/.env`
        * `echo AUTH_USER=MY_BASIC_AUTH_USER >> influx/.env`
        * `echo AUTH_PASSWORD_ENCRYPTED=MY_ENCRYPTED_BASIC_AUTH_PASSWORD >> influx/.env` (created with `htpasswd -nB $AUTH_USER`)
    * `ansible-playbook ansible-playbook.yaml`

# TODOs
* Obige Schritte via Ansible Playbook umsetzen
* HTTPS via traefik and Let's Encrypt (alternative: StepCA)
* Service: Startpage
* Service: blackbox-exporter to check whether outside services are available
* Increase Retention time (wie hoch kann man das ansetzen bis die Platte voll ist?)
* Alerting? Welchen Kanal? Telegram Channel?
* Healthchecks für die Services
* fritz-exporter ist nicht mehr verfügbar, eine der anderen Lösungen evaluieren und nutzen
* openweather soll mit dem lokalen InfluxDB laufen, muss noch umgestellt werden
